import React from 'react'
import Link from 'gatsby-link'

import { Container } from '../components/uielements'
import { Col, Row } from 'antd'

const AppHeader = () => (
  <header>
    <Container layout="standard" gutter={30}>
      <nav>
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/about">About</Link></li>
          <li><Link to="/contact">Contact</Link></li>
        </ul>
      </nav>
    </Container>
  </header>
)

export default AppHeader

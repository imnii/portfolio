import React from 'react'
import SectionWrapper from './section.style';

const Section = props => {
  return (
    <SectionWrapper>
      {props.children}
    </SectionWrapper>
  )
}

export default Section;

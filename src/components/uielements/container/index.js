import React from 'react'
import ContainerWrapper from './container.style';

const Container = props => {
  return (
    <ContainerWrapper className={`container-${props.layout}`} style={{ padding: `0 ${props.gutter / 2}px` }}>
      {props.children}
    </ContainerWrapper>
  )
}

export default Container;

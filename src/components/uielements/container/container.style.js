import styled from "styled-components";

export default styled.div`
    $.container-fluid {
        margin-right: auto;
        margin-left: auto;
        width: 100%;
    }
    &.container-standard {
        margin-right: auto;
        margin-left: auto;
        width: 100%;
        
        @media (min-width: 576px) {
            max-width: 540px;
        }
        @media (min-width: 768px) {
            max-width: 720px;
        }
        @media (min-width: 992px) {
            max-width: 960px;
        }
        @media (min-width: 1200px) {
            max-width: 1140px;
        }
    }
`;
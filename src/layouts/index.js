import React from 'react'
import PropTypes from 'prop-types'

import AppHeader from '../components/appHeader'
import './index.css'
import 'antd/dist/antd.min.css';

const Layout = ({ children, data }) => (
  <div>
    <AppHeader />
    <div>{children()}</div>
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`

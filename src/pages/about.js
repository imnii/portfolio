import React from 'react'
import Helmet from 'react-helmet'

// Components
import { Section, Container } from '../components/uielements'
import { Row, Col } from 'antd'

const AboutPage = () => (
  <div>
    <Helmet title={'About'} />
    <Section>
      <Container layout="standard" gutter={30}>
        <h1>About</h1>
        <Row type="flex" gutter={30}>
          <Col xs={{ span: 24 }} md={{ span: 12 }}>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </Col>
          <Col xs={{ span: 24 }} md={{ span: 12 }}>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </Col>
        </Row>
      </Container>
    </Section>
  </div>
)

export default AboutPage
